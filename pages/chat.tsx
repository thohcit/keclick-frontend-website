import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-chat h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Chat</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex flex-col-reverse md:flex-row mb-20 md:mb-[120px]'>
                <Image src="/imgs/chat/1.png" sizes="100vw" width="0" height="0"
                    className='md:w-[564px] w-full relative md:h-[512px] h-auto' alt="" />
                <div className='md:w-[calc(100%-564px)] md:pl-[40px] mb-5 md:mb-0'>
                    <p className='mb-2 text-xl font-bold md:text-2xl'>Live Chat</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        The live chat feature is a useful human resource tool. Not only does it support existing
                        employees, it also enhances workflow experience and more many benefits such as:
                    </p>
                    <p className='md:text-lg font-open-sans'>
                        Efficient online consultant experiences
                    </p>
                    <ul className='pl-5 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            You&apos;ll have the perfect tool to engage, assist and advise the candidates. The direct
                            communication channel allows you to help with application forms and provide quality
                            consultant expertise while the candidate is live onsite.
                        </li>
                    </ul>
                    <p className='md:text-lg font-open-sans'>
                        Reduce irrelevant applications
                    </p>
                    <ul className='pl-5 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Low-barrier communication channels allow candidates to ask quick qualifying questions they
                            have in mind, it also reduces the irrelevant application and time waste.
                        </li>
                    </ul>
                </div>
            </div>
            <div className='flex flex-col md:flex-row'>
                <div className='mb-5 md:pr-10 md:mb-0'>
                    <p className='mb-3 font-bold md:text-2xl'>Secured Chat for Internal Use</p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Get More Important Applications
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Streamline Applications
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Guide Candidates in the Right Direction
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Improve colleague communications
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Diversity infrastructure Conduct interviews
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Gather Feedback
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Support your workforce
                        </li>
                        <li className='md:text-lg font-open-sans'>
                            Prevent channel disruption
                        </li>
                    </ul>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/chat/2.png" sizes="100vw" width="0" height="0"
                        className='md:w-[626px] md:h-[706px] w-full relative h-auto' alt="" />
                </div>
            </div>

        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}