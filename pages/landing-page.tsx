import Image from "next/image";
import Layout from "../components/layout";
import Head from "next/head";
import Footer from "../components/footer";
import Link from "next/link";
export default function Home() {
  return (
    <>
      <Head>
        <title>Keclick</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        <div className="fixed top-0 z-50 flex items-center w-full h-20 bg-black bg-opacity-50">
          <div className="max-w-[1600px] mx-auto w-full md:px-20 px-5">
            <nav className="border-gray-210">
              <div className="flex items-center mx-auto ">
                <div className="w-[50px] h-[40px] relative">
                  <Link href="/">
                    <Image
                      className="w-[50px]"
                      src="/imgs/logo.svg"
                      alt=""
                      fill
                    />
                  </Link>
                </div>
                <div className="flex justify-center w-full">
                  <p className="md:text-xl font-bold text-xs text-white text-center">
                    Sign up now to get a free 30 days trial!
                  </p>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <section>
          <div className="bg-gray-110">
            <div className="bg-landing-page md:bg-contain bg-no-repeat">
              <div className="container mx-auto">
                <div className="flex md:flex-row flex-col item-center pt-[160px]">
                  <div className="md:w-[calc(100%-540px)] w-full">
                    <p className="md:text-80 text-2xl font-bold md:leading-[80px] md:mb-8 mb-4 md:tracking-[-5px]">
                      Sign up now to<br></br>get 30 days trial!
                    </p>
                    <p className="md:text-36 leading-[36px] text-lg">
                      Your all-in-one HR Software Solutions
                    </p>
                    <div className="w-full">
                      <Image
                        src="/imgs/about/2.png"
                        sizes="100vw"
                        width="0"
                        height="0"
                        className="md:w-[1050px] w-full relative  h-auto"
                        alt=""
                      />
                    </div>
                  </div>
                  <div className="md:w-[540px]">
                    <div className="mb-[30px] w-full">
                      <p className="mb-2 text-lg font-bold">Name</p>
                      <input
                        type="text"
                        className="w-full h-[50px] rounded-md p-[15px]"
                        placeholder="e.g. John Doe"
                      />
                    </div>
                    <div className="mb-[30px]">
                      <p className="mb-2 text-lg font-bold">Email</p>
                      <input
                        type="text"
                        className="w-full h-[50px] rounded-md p-[15px]"
                        placeholder="Email Address"
                      />
                    </div>
                    <div className="mb-[30px]">
                      <p className="mb-2 text-lg font-bold">Mobile No.</p>
                      <input
                        type="text"
                        className="w-full h-[50px] rounded-md p-[15px]"
                        placeholder="Subject Title"
                      />
                    </div>
                    <div className="mb-[30px]">
                      <p className="mb-2 text-lg font-bold">Enquiry</p>
                      <textarea
                        className="w-full h-[168px] rounded-md p-[15px]"
                        placeholder="Message"
                      ></textarea>
                    </div>
                    <Image
                      src="/imgs/capchar.png"
                      sizes="100vw"
                      width="0"
                      height="0"
                      className="md:w-[309px] w-full relative h-auto"
                      alt=""
                    />
                    <button className="w-full h-10 mb-5 text-white rounded-full bg-blue-210 mt-9">
                      Submit
                    </button>
                    <Image
                      src="/imgs/aws.png"
                      sizes="100vw"
                      width="0"
                      height="0"
                      className="md:w-[441px] w-full relative h-auto"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div>
                <div className="container py-12 mx-auto md:py-28">
                  <p className="mb-4 text-sm font-bold text-center text-purple-510">
                    We do more for your worlD
                  </p>
                  <p className="mb-16 text-2xl font-bold leading-[50px] text-center md:text-40">
                    All-in-one payroll and HR<br></br>solutions for you
                  </p>
                  <div className="flex flex-col items-center md:mb-20 md:items-start md:justify-between md:flex-row">
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/1.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">Interview</p>
                    </div>
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/2.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">
                        Employee<br></br>Management
                      </p>
                    </div>
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/3.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">Payroll</p>
                    </div>
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/4.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">
                        Assets Management
                      </p>
                    </div>
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/8.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">
                        Attendance
                      </p>
                    </div>
                  </div>
                  <div className="flex flex-col items-center md:items-start md:justify-between md:flex-row">
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/9.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">Claims</p>
                    </div>
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/5.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">Leaves</p>
                    </div>
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/6.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">
                        Appraisal Evaluation
                      </p>
                    </div>
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/7.png"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">Chat</p>
                    </div>
                    <div className="w-[150px] mb-5 md:mb-0">
                      <div className="w-full h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                        <div className="relative h-[50px] w-[50px]">
                          <Image
                            fill
                            className="h-[50px]"
                            src="/imgs/solution/1.svg"
                            alt=""
                          />
                        </div>
                      </div>
                      <p className="text-xl font-bold text-center">
                        Staff Mobile App
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer></Footer>
      </div>
    </>
  );
}
