import Image from "next/image";
import Layout from "../components/layout";
import { useEffect, useRef } from "react";
import { useCallback, useState } from "react";

import dynamic from "next/dynamic";
import React from "react";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";
const OwlCarousel = dynamic(import("react-owl-carousel"), { ssr: false });
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";

export default function Home() {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [enquiry, setEnquiry] = useState("");
    const [notification, setNotification] = useState("");
    const [errorMess, setErrorMess] = useState("");

    const { executeRecaptcha } = useGoogleReCaptcha();

    const handleSubmit = useCallback(
        (e: { preventDefault: () => void }) => {
            e.preventDefault();
            if (!executeRecaptcha) {
                console.log("Execute recaptcha not yet available");
                return;
            }
            executeRecaptcha("enquiryFormSubmit").then((gReCaptchaToken) => {
                console.log(
                    gReCaptchaToken,
                    "response Google reCaptcha server"
                );
                submitEnquiryForm(gReCaptchaToken);
            });
        },
        [executeRecaptcha]
    );
    const endpoint = "http://18.140.50.55:3000/smtp/email/contact-us";
    const submitEnquiryForm = (gReCaptchaToken: string) => {
        fetch("/api/contact", {
            method: "POST",
            headers: {
                Accept: "application/json, text/plain, */*",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                name: name,
                email: email,
                phone: phone,
                enquiry: enquiry,
                gRecaptchaToken: gReCaptchaToken,
            }),
        })
            .then((res) => res.json())
            .then((res) => {
                console.log(res, "response from backend");
                if (res?.status === "success") {
                    setNotification(res?.message);
                } else {
                    setErrorMess(res?.message);
                }
            });
    };
    return (
        <Layout>
            <section className="bg-top-banner h-screen md:h-[958px] bg-cover bg-center flex items-center pt-20">
                <div className="container mx-auto ">
                    <p className="text-[50px] md:text-[120px] leading-[70px] md:leading-[120px] font-bold text-white">
                        Payroll &<br></br>HR Solutions
                    </p>
                    <p className="text-white text-40 mb-6">
                        to take your business further
                    </p>
                    <a href="#" className="">
                        <p className="h-[60px] w-[212px] flex items-center justify-center font-open-sans bg-yellow-550 rounded-[20px] font-semibold text-sm">
                            Request A Demo
                        </p>
                    </a>
                </div>
            </section>
            <section className="bg-gray-110">
                <div className="container py-12 mx-auto md:py-28">
                    <p className="mb-4 text-sm font-bold text-center text-purple-510 uppercase">
                        We do more for your worlD
                    </p>
                    <p className="mb-16 text-2xl font-bold leading-[50px] text-center md:text-40">
                        All-in-one payroll and HR<br></br>solutions for you
                    </p>
                    <div className="flex flex-col items-center md:mb-20 md:items-start md:justify-between md:flex-row">
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/1.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Interview
                            </p>
                        </div>
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/2.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Employee Management
                            </p>
                        </div>
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/3.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Payroll
                            </p>
                        </div>
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/4.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Assets Management
                            </p>
                        </div>
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/8.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Attendance
                            </p>
                        </div>
                    </div>
                    <div className="flex flex-col items-center md:items-start md:justify-between md:flex-row">
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/9.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Claims
                            </p>
                        </div>
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/5.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Leaves
                            </p>
                        </div>
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/6.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Appraisal Evaluation
                            </p>
                        </div>
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/7.png"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Chat
                            </p>
                        </div>
                        <div className="mb-5 md:mb-0 md:w-1/5 flex flex-col items-center">
                            <div className="w-[150px] h-[150px] bg-white rounded-full flex items-center justify-center mb-9  drop-shadow-xs">
                                <Image
                                    src="/imgs/solution/1.svg"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[50px] relative h-auto"
                                    alt=""
                                />
                            </div>
                            <p className="text-xl font-bold text-center">
                                Staff Mobile App
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section className="bg-blue-310 bg-opacity-20">
                <div className="container py-12 mx-auto md:py-28">
                    <p className="mb-2 text-sm font-bold md:mb-7 text-purple-510">
                        WHAT WE ARE
                    </p>
                    <p className="font-bold text-40 mb-7">About Kelick</p>
                    <div className="flex flex-col items-end md:flex-row">
                        <div className="md:w-1/2">
                            <p className="md:text-lg font-open-sans mb-9">
                                Kelick&apos;s cloud-based software provides you
                                access to the entire employee lifecycle in the
                                company, incorporating vital features such as
                                interviews, the whole onboarding process,
                                employee management, payroll, attendance
                                tracking, and more.
                            </p>
                            <p className="md:text-lg font-open-sans mb-9">
                                Kelick&apos;s software simplified the workflow
                                and increases efficiency and drives productivity
                                across every aspect of your workflow.<br></br>
                                Kelick collects real-time data on every aspect
                                of your workforce from the daily clock in/out
                                hours, staff task progression for appraisal
                                evaluation, annual leave, medical leaves, and
                                more.
                                <br></br>
                                You will gain in-depth insights into project
                                team progression and individual performance for
                                making a better decision based on the collected
                                data and report from Kelick&apos;s all-in-one
                                solution.
                            </p>
                            <a href="#" className="hidden md:block">
                                <p className="text-sm font-semibold  bg-blue-210 w-[212px] text-white rounded-[20px] h-[60px] flex items-center justify-center font-open-sans">
                                    Read More
                                </p>
                            </a>
                        </div>
                        <div className="w-full md:w-1/2">
                            <div className="md:w-[720px] w-full h-[225px] md:h-[480px] md:mr-[-180px] relative">
                                <Image
                                    fill
                                    className="w-full mb-9 md:mb-0"
                                    src="/imgs/computer.png"
                                    alt=""
                                />
                            </div>
                            <div className="flex justify-center md:hidden">
                                <a href="#" className="">
                                    <p className="text-sm font-semibold font-open-sans bg-blue-210 w-[212px] text-white rounded-[20px] h-[60px] flex items-center justify-center">
                                        Read More
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="overflow-hidden bg-gray-110">
                <div className="container py-12 mx-auto md:py-28">
                    <p className="mb-4 text-sm font-bold text-center text-purple-510">
                        RECENT FEEDBACKS
                    </p>
                    <p className="mb-16 text-2xl font-bold text-center md:text-40 md:leading-[50px]">
                        Read what our clients<br></br>are saying
                    </p>
                    <div className="flex items-center justify-around">
                        <div className="md:max-w-[1000px] max-w-xs md:mx-auto">
                            <OwlCarousel
                                className="owl-theme"
                                loop
                                margin={10}
                                items={1}
                                dots={false}
                                nav={true}
                                navText={[
                                    '<Image fill src="/imgs/arrow-left.svg" alt="" />',
                                    '<Image fill src="/imgs/arrow-right.svg" alt=""/>',
                                ]}
                            >
                                <div className="flex flex-col px-5 md:flex-row item w-full">
                                    <div className="md:w-1/2 md:pr-5">
                                        <p className="text-lg mb-9">
                                            Kelick has streamlined our HR
                                            processes, making tasks such as
                                            tracking employee attendance and
                                            generating reports a breeze. The
                                            user-friendly interface and
                                            excellent customer support make it a
                                            top choice for any company.
                                        </p>
                                        <p className="font-bold text-22">
                                            Lim Xuan, HR Manager
                                        </p>
                                    </div>
                                    <Image
                                        src="/imgs/slide-1.png"
                                        sizes="100vw"
                                        width="0"
                                        height="0"
                                        className="md:!w-[560px] w-full relative h-auto"
                                        alt=""
                                    />
                                </div>
                                <div className="flex flex-col px-5 md:flex-row item">
                                    <div className="md:w-1/2 md:pr-5">
                                        <p className="text-lg mb-9">
                                            Kelick has streamlined our HR
                                            processes, making tasks such as
                                            tracking employee attendance and
                                            generating reports a breeze. The
                                            user-friendly interface and
                                            excellent customer support make it a
                                            top choice for any company.
                                        </p>
                                        <p className="font-bold text-22">
                                            Lim Xuan, HR Manager
                                        </p>
                                    </div>
                                    <Image
                                        src="/imgs/slide-1.png"
                                        sizes="100vw"
                                        width="0"
                                        height="0"
                                        className="md:!w-[560px] w-full relative h-auto"
                                        alt=""
                                    />
                                </div>
                            </OwlCarousel>
                        </div>
                        <div className="hidden md:block"></div>
                    </div>
                </div>
            </section>
            <section className="bg-yellow-lig">
                <div className="bg-right bg-contain md:bg-application py-12 md:pt-[100px] md:pb-[120px] bg-no-repeat">
                    <div className="container mx-auto ">
                        <p className="mb-2 text-sm font-bold text-purple-510">
                            PAYROLL & HR SERVICES
                        </p>
                        <p className="mb-2 font-bold text-2xl md:text-40  md:leading-[50px]">
                            One platform for all your HR
                            <br className="hidden md:block"></br>
                            management needs
                        </p>
                        <p className="md:text-lg font-open-sans font-semibold md:w-[640px] mb-9">
                            Manage everything related to your people from
                            onboarding to retirement from one cloud based HR
                            solution. Give employees access to their payroll,
                            benefits, and retirement plans from their mobile
                            device.
                        </p>
                        <div className="w-full md:hidden h-[190px] my-5 relative">
                            <Image fill src="/imgs/application.png" alt="" />
                        </div>
                        <div className="flex justify-around md:justify-start gap-10">
                            <a href="#">
                                <Image
                                    src="/imgs/google-play.png"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[150px] md:w-[220px]  relative h-auto"
                                    alt=""
                                />
                            </a>
                            <a href="#">
                                <Image
                                    src="/imgs/app-store.png"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="w-[150px] md:w-[220px]  relative h-auto"
                                    alt=""
                                />
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <section className="bg-purple-210 py-12 md:py-[100px]">
                <div className="container mx-auto">
                    <div className="grid md:grid-cols-5 gap-y-10 md:gap-x-10">
                        <div className="col-span-2">
                            <p className="mb-3 text-xl font-bold md:mt-32">
                                Kelick HQ
                            </p>
                            <div className="flex">
                                <div className="flex items-center mr-14">
                                    <Image
                                        width={24}
                                        height={24}
                                        src="/imgs/phone.svg"
                                        className="w-6 mr-2"
                                        alt=""
                                    />
                                    <p className="font-bold">8600 3811</p>
                                </div>
                                <div className="flex items-center">
                                    <Image
                                        width={24}
                                        height={24}
                                        src="/imgs/email.svg"
                                        className="w-6 mr-2"
                                        alt=""
                                    />
                                    <p className="font-bold">info@kelick.io</p>
                                </div>
                            </div>
                            <Image
                                width={500}
                                height={390}
                                src="/imgs/hr.png"
                                alt=""
                            />
                        </div>
                        <div className="col-span-3">
                            <form onSubmit={handleSubmit} method="post">
                                <p className="mb-2 text-sm font-bold text-purple-510">
                                    CONTACT US TODAY
                                </p>
                                <p className="mb-7 font-bold text-3xl md:text-40 leading-[50px]">
                                    Speak with us to know more
                                </p>
                                <div className="mb-[30px]">
                                    <p className="mb-2 text-lg font-bold">
                                        Name
                                    </p>
                                    <input
                                        required
                                        type="text"
                                        id="name"
                                        name="name"
                                        className="w-full h-[50px] rounded-md p-[15px]"
                                        placeholder="e.g. John Doe"
                                    />
                                </div>
                                <div className="mb-[30px]">
                                    <p className="mb-2 text-lg font-bold">
                                        Email
                                    </p>
                                    <input
                                        required
                                        type="email"
                                        id="email"
                                        name="email"
                                        className="w-full h-[50px] rounded-md p-[15px]"
                                        placeholder="Email Address"
                                    />
                                </div>
                                <div className="mb-[30px]">
                                    <p className="mb-2 text-lg font-bold">
                                        Mobile No.
                                    </p>
                                    <input
                                        required
                                        type="tel"
                                        id="phone"
                                        name="phone"
                                        className="w-full h-[50px] rounded-md p-[15px]"
                                        placeholder="Subject Title"
                                    />
                                </div>
                                <div className="">
                                    <p className="mb-2 text-lg font-bold">
                                        Enquiry
                                    </p>
                                    <textarea
                                        required
                                        id="enquiry"
                                        name="enquiry"
                                        className="w-full h-[168px] rounded-md p-[15px]"
                                        placeholder="Message"
                                    ></textarea>
                                </div>
                                <button className="w-full h-10 mb-5 font-semibold text-white rounded-full bg-blue-210 mt-9">
                                    Submit
                                </button>
                                {notification && (
                                    <p className="mb-5 font-bold text-green-700">
                                        {notification}
                                    </p>
                                )}
                                {errorMess && (
                                    <p className="mb-5 font-bold text-red-600">
                                        {errorMess}
                                    </p>
                                )}
                                <Image
                                    src="/imgs/aws.png"
                                    sizes="100vw"
                                    width="0"
                                    height="0"
                                    className="md:w-[441px] w-full relative h-auto"
                                    alt=""
                                />
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}
