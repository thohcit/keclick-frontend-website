import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section
        className="bg-employees-management h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Employees Management</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <p className='text-xl md:text-4xl font-bold text-center mb-10 md:mb-[60px]'>
                Gather feedback to gain actionable<br />data based on your employees&apos; morale
            </p>
            <div className='flex flex-col md:flex-row mb-[120px]'>
                <div className='md:w-[calc(100%-432px)] pr-16'>
                    <p className='mb-3 text-lg font-bold md:text-2xl'>Improve employee engagement at all level</p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Employees are free to share their views, and management can request feedback and have better
                            insights of the team.
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Plan and schedule employee engagement survey for feedback from company staff throughout the
                            year
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Collect and process feedback on events such as training programmes, internship, onboarding,
                            and promotions.
                        </li>
                    </ul>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/employees-detail.png" sizes="100vw" width="0" height="0"
                        className='md:w-[432px] md:h-[404px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row '>
                <Image src="/imgs/join-design.png" sizes="100vw" width="0" height="0"
                    className='md:w-[680px] w-full relative md:h-[393px] h-auto' alt="" />
                <div className='md:w-[calc(100%-680px)] md:pl-[60px]'>
                    <p className='mb-2 text-xl font-bold md:text-2xl'>Track Team Engagement</p>
                    <div>
                        <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Kelick software allows you to track employee engagement over time and identify areas for
                            improvement.</p>
                        <ul className='pl-5 list-disc'>
                            <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                                Conduct regular employee surveys to gauge job satisfaction
                            </li>
                            <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                                Gain real-time insight and have a better understanding of engagement trends by teams
                            </li>
                            <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                                Evaluate Insight and recognise high-performance employees and promote success
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}