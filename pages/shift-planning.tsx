import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-shift-planning h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Shift Planning</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex flex-col md:flex-row mb-[41px]'>
                <div className='md:w-[calc(100%-609px)] md:pr-10 mb-5 md:mb-0'>
                    <p className='mb-5 font-bold md:text-2xl'>Achieve Greater Flexibility with Real-Time <br /> Shift
                        Planning</p>
                    <p className='mb-5 md:text-lg font-open-sans md:mb-10'>
                        Real-Time Shift Planning feature gives you the flexibility to adjust your shift schedules
                        on-the-fly, ensuring your business can respond quickly to changing needs and demands. With our
                        intuitive shift planner, you can quickly create and modify shift schedules, view real-time shift
                        coverage information, and communicate with your employees in real-time.
                    </p>
                    <p className='md:text-lg font-open-sans'>Our Real-Time Shift Planning feature enables you to be
                        agile, adaptable, and responsive, improving your business&apos;s resilience and competitive
                        advantage in a fast-paced market.</p>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/shift-planning/1.png" sizes="100vw" width="0" height="0"
                        className='md:w-[609px] md:h-[264px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row'>
                <Image src="/imgs/shift-planning/2.png" sizes="100vw" width="0" height="0"
                    className='md:w-[720px] w-full relative md:h-[420px] h-auto md:ml-[-95px]' alt="" />
                <div className='md:w-[calc(100%-720px+60px)] md:pl-[30px] mb-5 md:mb-0'>
                    <p className='mb-5 text-xl font-bold md:text-2xl'>Optimize Your Workforce Management with <br />
                        Data-Driven Shift Planning</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Data-Driven Shift Planning feature leverages analytics and insights to help you create shift
                        schedules that maximize productivity and efficiency. With our intuitive reporting tools, you can
                        analyze employee performance metrics, identify workload gaps and inefficiencies, and create
                        optimized shift schedules that improve your workforce&apos;s performance.
                    </p>
                    <p className='md:text-lg font-open-sans'>
                        Our Data-Driven Shift Planning feature helps you make informed workforce decisions, eliminate
                        guesswork, and optimize your resources, resulting in a more productive and efficient business.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}