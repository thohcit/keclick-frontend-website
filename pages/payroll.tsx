import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-payroll h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Payroll</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex flex-col md:flex-row mb-20 md:mb-[120px]'>
                <div className='md:w-[50%] md:pr-14 mb-5 md:mb-0'>
                    <p className='mb-3 font-bold md:text-2xl'>Manage and pay your employees with just one system</p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            One glance at the dashboard and understand your entire workforce functionality.
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Kelick HR allows you to easily engage, manage and pay your staff.
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Secure company and employee information with the Kelick system
                        </li>
                        <li className='md:text-lg font-open-sans'>
                            Eliminate the possibility of any major mistake while paying and managing employees and
                            business changes such as new employee account setup, bonuses, claims and more.
                        </li>
                    </ul>
                </div>
                <div className='md:w-[50%] flex justify-end'>
                    <div className=''>
                        <Image src="/imgs/payroll/1.png?v=1" sizes="100vw" width="0" height="0"
                            className='w-full h-auto' alt="" />
                    </div>
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row mb-20 md:mb-[120px]'>
                <Image src="/imgs/payroll/2.png" sizes="100vw" width="0" height="0"
                    className='md:w-[650px] w-full relative md:h-[393px] h-auto' alt="" />
                <div className='md:w-[calc(100%-650px)] md:pl-[40px]  mb-5 md:mb-0'>
                    <p className='mb-2 text-xl font-bold md:text-2xl'>Kelick&apos;s Real-time analytics dashboard allows
                        you to access data whatever and whenever you required.</p>
                    <div>
                        <ul className='pl-5 list-disc'>
                            <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                                Monitor and edit every payroll at ease with Kelick&apos;s built-in analytic system
                            </li>
                            <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                                Track and report any changes in staff salary and bonuses as time pass.
                            </li>
                            <li className='md:text-lg font-open-sans'>
                                Submit payroll reports easily with easy-understandable dashboard static, charts, and
                                graphs
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className='flex flex-col md:flex-row mb-20 md:mb-[120px]'>
                <div className='md:w-[calc(100%-540px)] md:pr-14 md:mb-0 mb-5'>
                    <p className='mb-3 font-bold md:text-2xl'>Simplified Payroll process</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        No more dual entry on spreedsheets, enabled employees self-service, employees will be able to
                        check individual paystubs and benefit informations.
                    </p>
                    <p className='md:text-lg font-open-sans'>
                        Your HRIS and payroll are integrated and designed with ease of use in mind. However, if you have
                        any questions, you can look to our customer support team - we&apos;re here to help you
                        streamline your payroll process.
                    </p>
                </div>
                <div className='md:w-[540px] flex justify-end w-full'>
                    <div className=''>
                        <Image src="/imgs/payroll/3.png" sizes="100vw" width="0" height="0" className='w-full h-auto'
                            alt="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}