import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section
        className="bg-desk-room-booking h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Desk & Room Booking</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex flex-col md:flex-row mb-[90px]'>
                <div className='md:w-[calc(100%-651px)] md:pr-10 mb-5 md:mb-0 md:mt-[80px] mt-0'>
                    <p className='mb-5 font-bold md:text-2xl'>Streamline Your Workplace </p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Desk Booking feature allows employees to reserve available desks and workspaces ahead of time,
                        ensuring a smooth and organized office environment. With easy-to-use reservation tools and
                        customizable booking settings, employees can quickly find and book a desk that suits their
                        needs, whether they require a quiet workspace for focused work or a collaborative area for team
                        projects.
                    </p>
                    <p className='md:text-lg font-open-sans'>
                        Say goodbye to desk-hunting and hello to increased productivity and workplace satisfaction with
                        Desk Booking in our HR software.
                    </p>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/desk-room-booking/1.png" sizes="100vw" width="0" height="0"
                        className='md:w-[634px] md:h-[460px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='mb-[40px]'>
                <Image src="/imgs/desk-room-booking/2.png" sizes="100vw" width="0" height="0"
                    className='md:w-[1210px] w-full relative md:h-[311px] h-auto' alt="" />
            </div>
            <p className='mb-3 font-bold md:text-2xl'>Elevate Your Meeting Experience</p>
            <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                Room Booking feature enables employees to schedule and reserve available meeting rooms with ease,
                improving collaboration and productivity in the workplace. With an intuitive reservation system,
                real-time availability information, and customizable booking settings, employees can book the perfect
                meeting room for their needs, whether it&apos;s for a team brainstorming session or a client
                presentation.
            </p>
            <p className='md:text-lg font-open-sans'>
                Our Room Booking feature streamlines the meeting planning process, eliminates scheduling conflicts, and
                optimizes meeting room usage, providing a seamless and efficient meeting experience for all.
            </p>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}