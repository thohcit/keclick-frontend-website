import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-mobile-app h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Mobile App</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto '>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:w-[740px] md:pr-10 mb-5 md:mb-0 pt-[100px]'>
                    <p className='mb-3 font-bold md:text-2xl'>Manage Your HR Matter on Your Phone</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Much like the department itself, the value propositions of human resources applications are
                        changing. Administrative tools to ease data collection and enable employee self-service are
                        becoming table stakes—and leading organizations are leveraging HR software to optimize and more
                        deeply engage the workforce.
                        <br />
                        The benefit includes:
                    </p>
                    <ul className='grid grid-cols-1 pl-10 list-disc md:grid-cols-2 gap-x-8 gap-y-5'>
                        <li className='col-span-1 md:text-lg '>
                            Improve Efficiency and productivity
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Improve regulatory compliance
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Employee experience & morale
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Remote attendance monitoring
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Employee development & retention
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Simplified benefits administration
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Operation cost savings
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Data security
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Reduced unnecessary errors
                        </li>
                        <li className='col-span-1 md:text-lg'>
                            Data Metrics & Report
                        </li>
                    </ul>
                </div>
                <div className='md:h-[750px] h-[400px]'>
                    <Image src="/imgs/mobile-app/1.png?v=1" sizes="100vw" width="0" height="0"
                        className='md:w-[767px] md:h-[750px] w-full absolute right-0 h-auto' alt="" />
                </div>
            </div>

        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}