import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-attendance h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Attendance</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='md:px-[80px] mb-10 md:mb-[96px]'>
                <p className='font-bold text-center text-2xl md:text-36'>Keep track of attendance, work hours, and
                    overtime of your staff for easier remote management with Kelick</p>
            </div>
            <div className='flex flex-col md:flex-row mb-[52px]'>
                <div className='md:w-[calc(100%-580px)] md:pr-7 mb-5 md:mb-0'>
                    <p className='mb-3 font-bold md:text-2xl'>Workforce management made easy</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        One system to manage shift patterns, hours worked, absence and annual leave makes it easier for
                        you to effortlessly manage your entire workforce.
                    </p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Manage rotas, overtime, hours worked, holiday allowance and accruals, absence management and
                            much more from one system
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Unique QR codes ensure accurate timekeeping and prevent any fraudulent claims and time theft
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Monitor labour costs over time and create dynamic reports that illustrate key workforce
                            trends
                        </li>
                    </ul>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/attendance/1.png" sizes="100vw" width="0" height="0"
                        className='md:w-[580px] md:h-[488px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row mb-[70px]'>
                <Image src="/imgs/attendance/2.png" sizes="100vw" width="0" height="0"
                    className='md:w-[586px] w-full relative md:h-[413px] h-auto' alt="" />
                <div className='md:w-[calc(100%-586px)] md:pl-[40px] mb-5 md:mb-0 md:mt-[44px]'>
                    <p className='mb-2 text-xl font-bold md:text-2xl'>Simplify Time & Attendance Management</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Better manage the time keeping and time off of your workforce with an online system that allows
                        you to control labour costs and minimise any risk of mistakes with ease.
                    </p>
                    <ul className='pl-5 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Streamline time tracking with the accurate collection of employee time and attendance data
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Gain complete visibility of staff hours, attendance and sickness across multiple sites
                        </li>
                        <li className='md:text-lg font-open-sans'>
                            Support your compliance efforts with simple time scheduling to meet industry and working
                            time regulations
                        </li>
                    </ul>
                </div>
            </div>
            <div className='flex flex-col md:flex-row'>
                <div className='md:w-[calc(100%-522px)] md:pr-7 mb-5 md:mb-0'>
                    <p className='mb-3 font-bold md:text-2xl'>Engage your employees</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Provide your employees with the ability to manage their time off and attendance and allow
                        managers to view exactly who is working on any particular day and who is not.
                    </p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Track absenteeism trends and address any issues with employees before they become a problem
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Use unique QR codes to log accurate data on employee time and allow your remote staff to
                            clock in and out on any web-enabled device
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Access reports on time and attendance to identify trends and any problem areas in employee
                            absenteeism or sickness
                        </li>
                    </ul>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/attendance/3.png" sizes="100vw" width="0" height="0"
                        className='md:w-[522px] md:h-[348px] w-full relative h-auto' alt="" />
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}