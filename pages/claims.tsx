import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-claims h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Claims</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex flex-col-reverse md:flex-row mb-20 md:mb-[110px]'>
                <Image src="/imgs/claims/1.png" sizes="100vw" width="0" height="0"
                    className='md:w-[447px] w-full relative md:h-[290px] h-auto' alt="" />
                <div className='md:w-[calc(100%-447px)] md:pl-[70px] mb-5 md:mb-0'>
                    <p className='mb-2 text-xl font-bold md:text-2xl'>Claim expenses on the go</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Roubler&apos;s expense management software saves time and hassle by enabling employees to take
                        photos of receipts and submit expense claims straight away via the mobile app. Managers can also
                        quickly approve or decline expense requests on the go with a simple swipe left, swipe right.
                    </p>
                </div>
            </div>
            <div className='flex flex-col md:flex-row mb-[110px]'>
                <div className='md:w-[calc(100%-520px)] md:pr-[70px] mb-5 md:mb-0'>
                    <p className='mb-3 font-bold md:text-2xl'>Go paperless with Kelick software</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        With Kelick, there&apos;s no need for paper receipts and expense claim forms. Employees simply
                        scan and upload receipts on mobile or desktop. Their claim will be sent to their manager for
                        approval, before automatically synchronising with payroll for processing.
                    </p>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/claims/2.png" sizes="100vw" width="0" height="0"
                        className='md:w-[520px] md:h-[300px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row'>
                <Image src="/imgs/claims/3.png" sizes="100vw" width="0" height="0"
                    className='md:w-[546px] w-full relative md:h-[300px] h-auto' alt="" />
                <div className='md:w-[calc(100%-546px)] md:pl-[40px] mb-5 md:mb-0'>
                    <p className='mb-2 text-xl font-bold md:text-2xl'>Gain better business expenses insight</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Kelick&apos;s expense claims feature helps you manage business expenses more effectively. By
                        automating expense management, you will have access to accurate, detailed reports, and gain full
                        visibility overspend.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}