import "@/styles/globals.scss";
import type { AppProps } from "next/app";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";

export default function App({ Component, pageProps }: AppProps) {
    return (
        <GoogleReCaptchaProvider
            reCaptchaKey="6LezeWIlAAAAABmT5UazclN1QN1oVx1wRNHSbs5l"
            scriptProps={{
                async: false,
                defer: false,
                appendTo: "head",
                nonce: undefined,
            }}
        >
            <Component {...pageProps} />
        </GoogleReCaptchaProvider>
    );
}
