import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-leaves h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Leaves</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex flex-col md:flex-row mb-[100px]'>
                <div className='md:w-[calc(100%-405px)] md:pr-10 mb-5 md:mb-0'>
                    <p className='mb-5 font-bold md:text-2xl'>Track all type of leave</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        With All Hours you can easily track all types of leave, including annual leave, sick leave,
                        public holidays, private or unpaid leave, or the usage of surplus hours.
                    </p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Absence types can be completely customized and counted in days or hours as paid or unpaid time,
                        that way it&apos;s easy to track work done from home and other types of remote work.
                    </p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Paid and unpaid leave status
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Setup different rules for different employees
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Track remote work
                        </li>
                    </ul>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/leaves/1.png" sizes="100vw" width="0" height="0"
                        className='md:w-[405px] md:h-[376px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row mb-[40px]'>
                <div className='flex justify-center md:w-[calc(100%-580px)]'>
                    <Image src="/imgs/leaves/2.png" sizes="100vw" width="0" height="0"
                        className='md:w-[336px] w-full relative md:h-[434px] h-auto' alt="" />
                </div>

                <div className='md:w-[580px] mb-5 md:mb-0'>
                    <p className='mb-5 text-xl font-bold md:text-2xl'>Leave approvals with notifications</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        The approval process is fast, and the result is clear and visible to everyone
                        involved.Administrators get an email notification when a new request is made, and the employees
                        are notified when their request is approved or rejected.
                    </p>
                    <ul className='pl-5 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            No more endless email chains
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Fast approvals
                        </li>
                        <li className='md:text-lg font-open-sans'>
                            Transparent history of approvals
                        </li>
                    </ul>
                </div>
            </div>
            <div className='flex flex-col md:flex-row'>
                <div className='md:w-[calc(100%-480px)] md:pr-10 mb-5 md:mb-0'>
                    <p className='mb-3 font-bold md:mt-20 md:text-2xl'>Absence reports for payroll processing</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        This HR solution automatically includes and calculates all absences into your Payroll reports,
                        making the payroll process quick and painless often in real time. Absence reports can be
                        exported in CSV or Excel format.
                    </p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Reduce administration
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Automate payroll processing
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Customizable exports
                        </li>
                    </ul>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/leaves/3.png" sizes="100vw" width="0" height="0"
                        className='md:w-[480px] md:h-[495px] w-full relative h-auto' alt="" />
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}