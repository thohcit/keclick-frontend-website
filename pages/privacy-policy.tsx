import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <p className='text-center md:text-120 text-40 md:mb-[100px] mb-[40px] font-bold'>
                Privacy Policy
            </p>
            <div className="font-open-sans">
                <p className='mb-3 md:text-base md:mb-5'>
                    This Data Privacy Policy (“Privacy Policy”) sets out the basis in which Kelick (“ABPL”, “we”, “us”,
                    or “our”) may collect, use, disclose or otherwise process your personal data in accordance with the
                    Personal Data privacy Act (“PDPA”) of Singapore. This Privacy Policy applies to personal data in our
                    possession or under our control, including personal data in the possession of organizations which we
                    have engaged to collect, use, disclose or process personal data for such purposes which have been
                    notified. This Privacy Policy is read in conjunction with, and is deemed incorporated by reference,
                    to be part of our Terms and Conditions of Use.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    1. Personal
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    1.1. As used in this Privacy Policy, “personal data” means data, whether true or not, about an
                    individual who can be identified: (a) from that data; or (b) from that data and other information to
                    which we have or are likely to have access.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    1.2. Other terms used in this Privacy Policy shall have the meanings given to them in our Terms of
                    Use or the PDPA (where the context so permits).1.3. Depending on the nature of your interaction with
                    us, on the Sites or through any Service we provide, the type of information that we may collect
                    includes:
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • Personal data and contact information. We may collect information with personal identifiers
                    including your name and identification information such as your NRIC number, contact information
                    such as your address, email address or telephone number, nationality, gender, date of birth, marital
                    status, photographs and other audio-visual information.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • Employment details. We may collect information from your CV in relation to your occupation and
                    other positions held, employment history, salary, and/or benefits or education history.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • Financial information. We may collect financial information such as credit card numbers, debit
                    card numbers or bank account information and details of any other information that allows us to
                    transact with you and/or provide you with our Services.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • Device and Technical Information. We collect device-specific and other technical information about
                    your mobile or computer device such as the hardware model, operating system version, browser type
                    and version, Internet Protocol (IP) address, advertising identifier, unique application identifiers,
                    unique device identifiers, browser type, language, wireless network, and mobile network information
                    (including the mobile phone number).
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • Other Anonymized Data. We may collect information that is not associated with or linked to your
                    personal data and cannot be used to identify you or any other individuals (“Anonymized Data”).
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    1.4. Most information will be collected when you use the Sites or when you use the Services.
                    However, we may also use various technologies on the Sites which may lead to information being
                    collected automatically by us or from other sources such as advertising, sub-contractors in
                    technical and payment services, analytics providers, search information providers and mailing lists.
                    This information does not generally, but may, contain your personal data. In particular, information
                    is likely to be collected as follows:
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • Web beacons. Web beacons (also known as “clear gifs” and “pixel tags”) are small transparent
                    graphic images that are often used in conjunction with cookies in order to further personalize the
                    Service for our users, collect a limited set of information about our visitors, and advertise to our
                    users.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • Web analytics. Web analytics is the method for collecting and assessing the behavior of users on
                    the Sites. This includes the analysis of traffic patterns in order, for example, to determine the
                    frequency of visits to certain parts of a website or mobile application, or to find out what
                    information and services the users are most interested in. We may also use web beacons in email
                    communications in order to understand the behavior of our users, such as whether an email has been
                    opened or acted upon.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • Cookies. We use cookies on the Sites, which are small text files that a website stores on your PC,
                    telephone or any other device with information on your navigation on the Sites, whose principal
                    objective is to improve your experience on the Sites, track use of the Sites and to provide us
                    subsequently with aggregated information of these activities for statistical purposes. By default,
                    cookies are enabled on web browsers but if you prefer, you may disable the use of cookies in your
                    browser and delete the cookies saved in your browser associated with the Sites.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2. Collection, use and disclosure of personal data
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2.1. As there are many circumstances in which we may collect information, we will endeavor to ensure
                    that you are always aware of the information being collected, in particular by third parties. We
                    generally do not collect your personal data unless
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • it is provided to us voluntarily by you directly or via a third party who has been duly authorized
                    by you to disclose your personal data to us (your “authorized representative”, for example Facebook
                    or any other non ABPL digital platform) after (a) you (or your authorized representative) have been
                    notified of the purposes for which the data is collected, and (b) you (or your authorized
                    representative) have provided consent to the collection and usage of your personal data for those
                    purposes, or
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • collection and use of personal data without consent is permitted or required by the PDPA or other
                    laws. We shall seek your consent before collecting any additional personal data and before using
                    your personal data for a purpose which has not been notified to you (except where permitted or
                    authorized by law).
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2.2. We may collect and use your personal data for any or all of the following purposes:
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • performing obligations in the course of or in connection with our provision of the Services
                    requested by you; <br />
                    • verifying your identity; <br />
                    • responding to, handling, and processing queries, requests, applications, complaints, and feedback
                    from you; <br />
                    • detecting usage patterns; <br />
                    • troubleshooting crashes associated with specific hardware and software used to access the Sites
                    and Services; <br />
                    • analyzing trends; <br />
                    • administering the sites; <br />
                    • tracking user movements; and• gathering broad demographic information for aggregate use of the
                    Services; <br />
                    • processing payment or credit transactions; <br />
                    • sending you marketing information about the Services on behalf of ourselves and through our
                    sponsors or other third-party service providers, including notifying you of advertisements,
                    marketing events and other promotions; complying with any applicable laws, regulations, codes of
                    practice, guidelines, or rules, or to assist in law enforcement and investigations conducted by any
                    governmental and/or regulatory authority; <br />
                    • any other purposes for which you have provided the information; <br />
                    • transmitting to any unaffiliated third parties including our third-party service providers and
                    agents, and relevant governmental and/or regulatory authorities, whether in Singapore or abroad, for
                    the aforementioned purposes; and <br />
                    • any other incidental business purposes related to or in connection with the above. <br />
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2.3. Your personal data may also be anonymized for use by ABPL and its affiliates for other
                    purposes.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2.4. It may be necessary for us to disclose your personal data to our affiliates or to third parties
                    in a manner compliant with the PDPA in order to carry out the purposes set out above. We may
                    disclose or share personal data with such parties who provide necessary services to us, such as for
                    processing activities like verification, website hosting, data analytics and payment processing. We
                    may disclose your personal data in the following circumstances:
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    • where such disclosure is required for performing obligations in the course of or in connection
                    with our provision of the Services requested by you; <br />
                    • third party online payment processing companies (e.g. Stripe) who will process your payments so
                    that you can use the Services and/or make any related purchases; <br />
                    • our consultants and professional advisers (e.g. accounting firms, audit firms, law firms and/or
                    other professional advisory firms) who are maintaining our records in accordance with legal
                    requirements; <br />
                    • governmental, regulatory or law enforcement bodies who have a legal right to demand for your
                    personal data, in response to a legal request such as a court order, to investigate or report an
                    illegal activity, or to enforce our rights or defend claims; <br />
                    • our affiliated or related companies and/or third parties to whom we may sell, divest, transfer,
                    assign, share or otherwise engage in a transaction that involves, some or all of the our assets
                    (which may include your personal data), in the course of a corporate divestiture, corporate
                    restructuring, merger, acquisition, joint venture, bankruptcy, dissolution, reorganization, or any
                    other similar transaction or proceeding; or <br />
                    • third party service providers, agents and other organizations we have engaged to perform any of
                    the functions listed in clause 2.2 above for us. <br />
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2.5. We do not sell, rent, license or otherwise disclose your personal data to third parties. We are
                    careful in our selection of third-party service providers, and when we disclose your personal data
                    to such parties, we shall require them to ensure that they are bound by obligations of data privacy
                    (pursuant to contracts and/or applicable laws) similar to the obligations as set out in the PDPA.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2.6. We may also disclose aggregated information that is Anonymized Data about our users, including
                    but not limited to the total number of our users and their overall demographic.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2.7. The Sites may have the functionalities to allow you to share personal data with other third
                    parties such as other users of the Sites. You are responsible for your choice(s) and are deemed to
                    have provided consent for any sharing of your personal data in the manner provided by the Sites.
                    Please do not post or upload personal data to the Sites that you would not want to be publicly
                    available.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    2.8. The purposes listed in the above clauses may continue to apply even in situations where your
                    relationship with us (for example, pursuant to a contract) has been terminated or altered in any
                    way, for a reasonable period thereafter (including, where applicable, a period to enable us to
                    enforce our rights under any contract with you).
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    3. Transfers of Personal Data Outside of Singapore
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    Pursuant to the purposes and activities as set out herein, it may be necessary for us to transfer
                    your personal data out of Singapore for storage, processing and use by ABPL, its affiliates or any
                    ABPL Sponsors (as defined in the Terms and Conditions of Use). You hereby agree and consent to the
                    aforesaid transfer, on the understanding that the recipients of such personal data are also subject
                    to applicable laws or contractual obligations similar to the obligations as set out in the PDPA.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    4. Withdrawing your consent
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    4.1. The consent that you provide for the collection, use and disclosure of your personal data will
                    remain valid until such time it is being withdrawn by you in writing. You may withdraw consent and
                    request us to stop using and/or disclosing your personal data for any or all of the purposes listed
                    above by submitting your request in writing or via email to our Data Privacy Officer at the contact
                    details provided in clause 10.1 below.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    4.2. Upon receipt of your written request to withdraw your consent, we may require reasonable time
                    (depending on the complexity of the request and its impact on our relationship with you) for your
                    request to be processed and for us to notify you of the consequences of us acceding to the same,
                    including any legal consequences which may affect your rights and liabilities to us. In general, we
                    shall seek to process your request within ten (10) business days of receiving it.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    4.3. Whilst we respect your decision to withdraw your consent, please note that depending on the
                    nature and scope of your request, we may not be in a position to continue providing our services to
                    you and we shall, in such circumstances, notify you before completing the processing of your
                    request. Should you decide to cancel your withdrawal of consent, please inform us in writing in the
                    manner described in clause 10.1 below.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    4.4. Please note that withdrawing consent does not affect our right to continue to collect, use and
                    disclose personal data where such collection, use and disclose without consent is permitted or
                    required under applicable laws.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    5. Access to and Correction of Personal Data
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    5.1. You may request access or make corrections to the personal data which we hold about you. You
                    may also request information about the ways in which we use, process or disclose your personal data.
                    Please submit your request in writing or via email to our Data privacy Officer at the contact
                    details provided in clause 10.1 below.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    5.2. We may charge a reasonable fee for processing your request for access. If so, we will inform
                    you of the fee before processing your request.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    5.3. We will respond to your request as soon as reasonably possible. Should we not be able to
                    respond to your request within thirty (30) days after receiving your request, we will inform you in
                    writing within thirty (30) days of the time by which we will be able to respond to your request. If
                    we are unable to provide you with any personal data or to make a correction requested by you, we
                    shall generally inform you of the reasons why we are unable to do so (except where we are not
                    required to do so under the PDPA).
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    6. Protection of Personal Data
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    6.1. We take all appropriate precautions to safeguard your personal data from unauthorized access,
                    collection, use, disclosure, copying, modification, disposal or similar risks. Examples of measures
                    we have implemented include encryption, privacy filters and up-to-date antivirus software to protect
                    and secure all storage and transmission of your personal data.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    6.2. You should be aware, however, that no method of transmission over the Internet and no
                    electronic storage is completely secure. You understand that any message or information that you
                    send, transmit or upload to the Sites may be read or intercepted by others, even if there is a
                    special notice that a particular transmission is encrypted. While security cannot be guaranteed, we
                    strive to protect the security of your personal data by constantly reviewing and enhancing our
                    security measures. However, we cannot accept responsibility for misuse or loss of, or unauthorized
                    access to your personal data.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    7. Disclaimer <br />
                    To the fullest extent permitted by law, we shall not be liable in any event for any special,
                    exemplary, punitive, indirect, incidental or consequences of damages of any kind or for any loss of
                    reputation or goodwill, whether based in contract, tort (including negligence), equity, strict
                    liability, statute or otherwise, suffered as a result of unauthorized or unintended use, access or
                    disclosure of your personal data.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    8. Accuracy of Personal Data <br />
                    We generally rely on personal data provided by you (or your authorized representative). In order to
                    ensure that your personal data is current, complete and accurate, please update us if there are
                    changes to your personal data by informing our Data Privacy Officer in writing or via email at the
                    contact details provided in clause 10.1 below.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    9. Retention of Personal Data
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    9.1. We may retain your personal data for as long as it is necessary to fulfil the purposes for
                    which it was collected, or as required or permitted by applicable laws.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    9.2. We will cease to retain your personal data, or remove the means by which the data can be
                    associated with you, as soon as it is reasonable to assume that such retention no longer serves the
                    purpose for which the personal data was collected, and is no longer necessary for legal or business
                    purposes.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    10. Data Privacy Officer
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    10.1. You may contact our Data Privacy Officer if you have any complaints, enquiries or feedback on
                    this Privacy Policy, your personal data as registered with us, or in relation to our use, retention,
                    disclosure or transfer of your personal data, or if you wish to make any request, in the following
                    manner: <br />
                    Attn: Kelick Data Privacy Officer <br />
                    Email: info@kelick.com
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    10.2. All requests for correction or for access to your personal data must be in writing. We will
                    endeavor to respond to your request within 30 days, and if that is not possible, we will inform you
                    of the time by which we will respond to you.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    10.3. All complaints will be evaluated by ABPL in a timely manner. After ABPL has completed its
                    evaluation, our Data privacy Officer (or duly appointed representative) will respond to the person
                    who submitted the complaint or feedback, with the results of the evaluation.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    10.4. We may be prevented by law from complying with any request that you make. We may also decline
                    any request that you may make if the law permits us to do so.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    11. Effect of Privacy Policy and changes to the Privacy Policy
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    11.1. This Privacy Policy applies in conjunction with any other notices, contractual clauses and
                    consent clauses that apply in relation to the collection, use and disclosure of your personal data
                    by us.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    11.2. We may revise this Privacy Policy from time to time without any prior notice. You may
                    determine if any such revision has taken place by referring to the date on which this Privacy Policy
                    was last updated. Your continued use of our services constitutes your acknowledgement and acceptance
                    of such changes.
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    12. Governing Law
                </p>
                <p className='mb-3 md:text-base md:mb-5'>
                    The terms and conditions of this Privacy Policy are governed by the laws of the Republic of
                    Singapore. In the event of any disputes arising from this Privacy Policy, you must first contact our
                    Data Privacy Officer regarding your concern and use your best endeavors to amicably settle any
                    dispute in good faith. We on our part will also use our best endeavors to amicably settle your
                    concerns in good faith. However, if no amicable resolution is reached within 30 days, you agree to
                    submit to the exclusive jurisdiction of the Courts of the Republic of Singapore.
                </p>
            </div>
        </div>
    </section>
</Layout>
);
}