import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-contact h-screen flex items-center md:h-[680px] bg-cover bg-center ">
        <div className='container mx-auto'>
            <div className='flex flex-col items-center justify-center'>
                <p className="text-[50px] md:text-[120px] leading-[70px] md:leading-[140px] font-bold text-white  ">
                    Contact</p>
                <p className='mb-3 text-2xl text-white md:text-40 md:mb-4'>
                    Speak with us to know more information
                </p>
            </div>
        </div>
    </section>
    <section className="bg-gray-110 py-12 md:py-[100px]">
        <div className="container mx-auto">
            <div className="flex justify-center">
                <div className="">
                    <p className="mb-3 text-xl font-bold md:mt-32">Kelick HQ</p>
                    <div className="flex">
                        <div className="flex items-center mr-14">
                            <Image width={24} height={24} src="/imgs/phone.svg" className="w-6 mr-2" alt="" />
                            <p className="font-bold">8600 3811</p>
                        </div>
                        <div className="flex items-center">
                            <Image width={24} height={24} src="/imgs/email.svg" className="w-6 mr-2" alt="" />
                            <p className="font-bold">info@kelick.io</p>
                        </div>
                    </div>
                    <Image width={500} height={390} src="/imgs/hr.png" alt="" />
                </div>
                <div className="md:w-[680px">
                    <p className="mb-2 text-sm font-bold text-purple-510">CONTACT US TODAY</p>
                    <p className="mb-7 font-bold text-3xl md:text-40 leading-[50px]">Speak with us to know more</p>
                    <div className="mb-[30px]">
                        <p className="mb-2 text-lg font-bold">Name</p>
                        <input type="text" className="w-full h-[50px] rounded-md p-[15px]"
                            placeholder="e.g. John Doe" />
                    </div>
                    <div className="mb-[30px]">
                        <p className="mb-2 text-lg font-bold">Email</p>
                        <input type="text" className="w-full h-[50px] rounded-md p-[15px]"
                            placeholder="Email Address" />
                    </div>
                    <div className="mb-[30px]">
                        <p className="mb-2 text-lg font-bold">Mobile No.</p>
                        <input type="text" className="w-full h-[50px] rounded-md p-[15px]"
                            placeholder="Subject Title" />
                    </div>
                    <div className="mb-[30px]">
                        <p className="mb-2 text-lg font-bold">Enquiry</p>
                        <textarea className="w-full h-[168px] rounded-md p-[15px]" placeholder="Message"></textarea>
                    </div>
                    <Image src="/imgs/capchar.png" sizes="100vw" width="0" height="0"
                        className='md:w-[309px] w-full relative h-auto' alt="" />
                    <button className="w-full h-10 mb-5 text-white rounded-full bg-blue-210 mt-9">Submit</button>
                    <Image src="/imgs/aws.png" sizes="100vw" width="0" height="0"
                        className='md:w-[441px] w-full relative h-auto' alt="" />
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}