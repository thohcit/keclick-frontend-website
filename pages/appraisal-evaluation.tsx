import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section
        className="bg-appraisal-evaluation h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Appraisal Evaluation</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex flex-col md:flex-row mb-[120px]'>
                <div className='md:w-[calc(100%-686px)] md:pr-10 mb-5 md:mb-0'>
                    <p className='mb-5 font-bold md:text-2xl'>Track your team&apos;s performance by creating a
                        tailor-made performance evaluation</p>
                    <p className='mb-2 md:mb-3 md:text-lg font-open-sans'>
                        Create custom-made questionnaires
                    </p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Create customizable evaluations with your own goals or abilities, you can use them whenever
                            you need them.
                        </li>
                    </ul>
                    <p className='mb-2 md:mb-3 md:text-lg font-open-sans'>
                        Choose the answer type
                    </p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Choose the answer type that best suits your methodology, write your own questions or use
                            preset ones.
                        </li>
                    </ul>
                    <p className='mb-2 md:mb-3 md:text-lg font-open-sans'>
                        Schedule their sending
                    </p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Prepare your performance evaluations and schedule them. You can track them and send
                            reminders to employees pending to answer.
                        </li>
                    </ul>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/appraisal-evaluation/1.png" sizes="100vw" width="0" height="0"
                        className='md:w-[686px] md:h-[645px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row'>
                <Image src="/imgs/appraisal-evaluation/2.png" sizes="100vw" width="0" height="0"
                    className='md:w-[542px] w-full relative md:h-[400px] h-auto' alt="" />
                <div className='md:w-[calc(100%-542px)] md:pl-[40px] mb-5 md:mb-0'>
                    <p className='mb-2 text-xl font-bold md:text-2xl'>Schedule one to ones and improve internal
                        communication with your team</p>
                    <ul className='pl-5 list-disc'>
                        <li className='mb-2 md:mb-3 md:text-lg font-open-sans'>
                            Detect improvement opportunities in advance and address future complications.
                        </li>
                        <li className='mb-2 md:mb-3 md:text-lg font-open-sans'>
                            Share improvement opportunities with your employees and boost their performance in the
                            company.
                        </li>
                        <li className='mb-2 md:mb-3 md:text-lg font-open-sans'>
                            Offer plans for growth and motivate your staff.
                        </li>
                        <li className='md:text-lg font-open-sans'>
                            Promote communication and increase your team&apos;s confidence.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#">
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}