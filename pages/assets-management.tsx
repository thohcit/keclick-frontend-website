import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section
        className="bg-asset-management h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p
            className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36 text-center">
            Assets Management</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex flex-col md:flex-row mb-[120px]'>
                <div className='md:w-[calc(100%-420px)] md:pr-10 mb-5 md:mb-0'>
                    <p className='mb-3 font-bold md:text-2xl'>Provide on-demand view of company assets</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Collect and store information about your business assets and inventory in the central cloud
                        system, track assigned assets, log details about an asset, and more.<br />
                        Our asset tracking software offer:
                    </p>
                    <ul className='pl-10 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Keep track of all assigned assets that have been handed over to user and know which are
                            available for issue.
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Supervise the condition, warranty, and purchase price of your assets
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Reduce admin time process by setting up automated asset management.
                        </li>
                        <li className='md:text-lg font-open-sans'>
                            Monitor asset usage and ensure compliance
                        </li>
                    </ul>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/assets-management/1.png" sizes="100vw" width="0" height="0"
                        className='md:w-[420px] md:h-[420px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row mb-20 md:mb-[120px]'>
                <Image src="/imgs/assets-management/2.png" sizes="100vw" width="0" height="0"
                    className='md:w-[634px] w-full relative md:h-[460px] h-auto' alt="" />
                <div className='md:w-[calc(100%-634px)] md:pl-[40px] mb-5 md:mb-0'>
                    <p className='mb-2 text-xl font-bold md:text-2xl'>Comprehensive inventory detail and lifespan</p>
                    <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                        Kelick HR allow you effectively track and maintenance schedules for your assets and ensure
                        assets are at its top performance.
                    </p>
                    <ul className='pl-5 list-disc'>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Understand which assets require updates or maintenance in order to extend the life of your
                            assets
                        </li>
                        <li className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Set automated reminders that notify you of expiration dates and licence renewals and, in the
                            case of company cars, remind you when MOT, tax, roadside cover or insurance is due for
                            renewal
                        </li>
                        <li className='md:text-lg font-open-sans'>
                            Minimise the risk of asset theft or fraudulent activity (i.e. a departing employee returns a
                            different mobile device or model of PC) with a comprehensive asset list, and quickly resolve
                            instances of misplace asset list and quickly resolve instances of misplaced assets
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}