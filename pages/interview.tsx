import Image from 'next/image'
import Layout from '../components/layout'
import { useEffect, useRef } from "react";
import dynamic from "next/dynamic";
import React from 'react';
const OwlCarousel = dynamic(import("react-owl-carousel"), {ssr: false});
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
export default function Home() {
return (
<Layout>
    <section className="bg-interview h-screen md:h-[680px] bg-cover bg-center flex justify-center items-end pt-20">
        <p className="text-[50px] md:text-[100px] leading-[70px] md:leading-[120px] font-bold text-white mb-36">
            Interview</p>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <div className='flex md:flex-row flex-col mb-[100px]'>
                <div className='md:w-[50%]'>
                    <p className='mb-2 text-lg font-bold md:text-2xl'>Talents consistency</p>
                    <div>
                        <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Finding the right person that fits into your high-performing project team can be a difficult
                            task. Therefore, it is vital to ensure the quality people you interview is qualified and a
                            good fit for the team.
                        </p>
                        <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Despite the wealth of evidence around the benefits of a structured, criteria-based approach
                            to interviewing, many managers are still content to rely on gut feeling and instinct when
                            recruiting. This approach is even riskier now with the need to ensure compliance with
                            diversity and equality legislation.
                        </p>
                        <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            With our interview platform, we utilise a range of features to ensure that your recruitment
                            team attracts the best talent available.
                        </p>
                    </div>
                </div>
                <div className='md:w-[50%] flex justify-end'>
                    <Image src="/imgs/talent.png" sizes="100vw" width="0" height="0"
                        className='md:w-[519px] w-full relative md:h-[480px] h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row '>
                <Image src="/imgs/hand-shake.png" sizes="100vw" width="0" height="0"
                    className='md:w-[513px] w-full relative md:h-[460px] h-auto' alt="" />
                <div className='md:w-[calc(100%-513px)] md:pl-[67px]'>
                    <p className='mb-2 text-lg font-bold md:text-2xl'>Track interviewee successes</p>
                    <div>
                        <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Interview notes, scores, or given assessment results can be captured in the Kelick interview
                            system to provide an auditable record of decisions in the event of a challenge by an
                            unsuccessful candidate. Once the result is concluded, offer or rejection emails can be sent
                            to each candidate, based on the client-editable templates.</p>
                        <p className='mb-5 md:mb-10 md:text-lg font-open-sans'>
                            Any offers can be tracked and closed, with the information captured during the recruitment
                            process automatically transferred to the employee database for successful candidates. This
                            then automatically feeds into the new starter process, with any data on unsuccessful
                            candidates deleted when no longer required, ensuring compliance with data protection laws.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans '>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}