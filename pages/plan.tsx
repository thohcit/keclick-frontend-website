import Image from "next/image";
import Layout from "../components/layout";
export default function Home() {
return (
<Layout>
    <section className="bg-plan h-screen flex items-center md:h-[680px] bg-cover bg-center ">
        <div className="container mx-auto">
            <div className="flex flex-col items-center justify-center">
                <p className="text-[50px] md:text-[120px] leading-[70px] md:leading-[140px] font-bold text-white ">
                    Plans
                </p>
                <p className="mb-3 text-2xl text-white md:text-40 md:mb-4">
                    View the various plans we offer!
                </p>
            </div>
        </div>
    </section>
    <section className="bg-gray-110">
        <div className="container mx-auto py-[90px]">
            <p className="md:text-72 font-bold text-36 mb-5 text-center">Plans</p>
            <p className="md:text-36 text-xl mb-9 font-bold text-gray-810 text-center">
                Choose the best plan for your company
            </p>
            <div className="flex md:flex-row flex-col items-center justify-center">
                <div
                    className="bg-white rounded-xl md:mr-10 shadow-gray-210 w-full md:w-[420px] md:h-[720px] p-7 md:p-10 flex flex-col justify-between mb-5 md:mb-0">
                    <div>
                        <p className="text-blue-310 font-bold md:text-3xl text-lg md:pl-5 mb-2 md:mb-5">
                            Enterprise Plan
                        </p>
                        <p className="md:text-xl font-bold md:pl-5 mb-5 text-gray-950">Features</p>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">
                                Admin Dashboard Management Control
                            </p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Interview Module</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">
                                Employees Management System
                            </p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Payroll Module</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">
                                Assets Management Module
                            </p>
                        </div>
                    </div>
                    <div className="flex justify-center">
                        <button className="bg-blue-310 rounded-full w-1/2 h-[40px] flex justify-center items-center">
                            <span className="text-sm text-white font-bold font-open-sans">
                                Book A Demo
                            </span>
                        </button>
                    </div>
                </div>
                <div
                    className="bg-white rounded-xl shadow-gray-210 w-full md:w-[420px] md:h-[720px] p-7 md:p-10 flex flex-col justify-between">
                    <div>
                        <p className="text-blue-310 font-bold md:text-3xl text-lg md:pl-5 mb-2 md:mb-5">
                            Corporate Plan
                        </p>
                        <p className="md:text-xl font-bold md:pl-5 mb-5 text-gray-950">Features</p>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">
                                Admin Dashboard Management Control
                            </p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Interview Module</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">
                                Employees Management System
                            </p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Payroll Module</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">
                                Assets Management Module
                            </p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Attendance Module</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Claims Module</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Leaves Module</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">
                                Appraisal Evaluation Module
                            </p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Chat Feature</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <Image width={24} height={24} src="/imgs/check_24px.svg" className="w-6 h-6 mr-5" alt="" />
                            <p className="md:text-base text-xs font-open-sans">Staff Mobile App</p>
                        </div>
                    </div>
                    <div className="flex justify-center">
                        <button className="bg-blue-310 rounded-full w-1/2 h-[40px] flex justify-center items-center">
                            <span className="text-sm text-white font-bold font-open-sans">
                                Book A Demo
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}