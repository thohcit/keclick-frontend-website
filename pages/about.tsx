import Image from 'next/image'
import Layout from '../components/layout'
export default function Home() {
return (
<Layout>
    <section className="bg-about h-screen flex items-center md:h-[680px] bg-cover bg-center ">
        <div className='container mx-auto'>
            <div className='flex flex-col justify-center'>
                <p className="text-[50px] md:text-[120px] leading-[70px] md:leading-[140px] font-bold text-white  mb-7">
                    About</p>
                <p className='mb-3 text-2xl text-white md:text-40 md:mb-4'>
                    Kelick has HR solutions to fit the needs of any size business.
                </p>
                <p className='mb-3 text-2xl text-white md:text-40 md:mb-4'>
                    From do it yourself payroll to human capital management, <br />
                </p>
                <p className='mb-3 text-2xl text-white md:text-40 md:mb-4'>
                    We have exactly what you need for your business.
                </p>
            </div>
        </div>
    </section>
    <section className='bg-gray-110'>
        <div className='container mx-auto py-10 md:py-[100px]'>
            <p className="mb-2 text-sm font-bold md:mb-7 text-purple-510">WHAT WE ARE</p>
            <div className='flex flex-col md:flex-row mb-[120px]'>
                <div className='md:w-[calc(100%-461px)] md:pr-10 mb-5 md:mb-0'>
                    <p className='text-2xl font-bold mb-7 md:text-40'>About Kelick</p>
                    <p className='mb-5 md:mb-10 md:text-xl font-open-sans'>
                        At Kelick, we restructure the way people think and work, enabling their organisation to advance
                        and thrive for better results. We simplify the processes and automate HR Chores and services
                        with the most exquisite features demanded in the human resource industries.
                    </p>
                    <p className='mb-5 md:mb-10 md:text-xl font-open-sans'>
                        Being one of the leading industry&apos;s human resource management system, we offer affordable
                        prices with essential modern human resource features that suit every business niche. Our support
                        team do not offer redundant features which might not be useful for your organisation.
                    </p>
                    <p className='mb-5 md:mb-10 md:text-xl font-open-sans'>
                        The support team will guide you to make the right choice while buying our human resource
                        management system. We are confident that the services provided to businesses will surely build a
                        trustworthy relationship that could last for years.
                    </p>
                </div>
                <div className='flex justify-end'>
                    <Image src="/imgs/about/1.png" sizes="100vw" width="0" height="0"
                        className='md:w-[461px] md:h-[423px] w-full relative h-auto' alt="" />
                </div>
            </div>
            <div className='flex flex-col-reverse md:flex-row md:mb-[120px]'>
                <Image src="/imgs/about/2.png" sizes="100vw" width="0" height="0"
                    className='md:w-[805px] w-full relative md:h-[648px] h-auto md:ml-[-60px]' alt="" />
                <div className='md:w-[calc(100%-634px)] mb-5 md:mb-0'>
                    <p className='mb-5 text-2xl font-bold md:text-40'>Our Software</p>
                    <p className='mb-5 md:mb-10 md:text-xl font-open-sans'>
                        Kelick&apos;s cloud-based software provides you access to the entire employee lifecycle in the
                        company, incorporating vital features such as interviews, the whole onboarding process, employee
                        management, payroll, attendance tracking, and more.
                    </p>
                    <p className='mb-5 md:mb-10 md:text-xl font-open-sans'>
                        Kelick&apos;s software simplified the workflow and increases efficiency and drives productivity
                        across every aspect of your workflow. <br />
                        Kelick collects real-time data on every aspect of your workforce from the daily clock in/out
                        hours, staff task progression for appraisal evaluation, annual leave, medical leaves, and more.
                    </p>
                    <p className='md:text-xl font-open-sans'>
                        You will gain in-depth insights into project team progression and individual performance for
                        making a better decision based on the collected data and report from Kelick&apos;s all-in-one
                        solution.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section className='bg-yellow-lig pt-[60px] pb-[60px]'>
        <div className='container mx-auto'>
            <div className='relative flex flex-col md:flex-row'>
                <div className='md:pt-[60px] flex flex-col items-center md:items-start mb-10 md:mb-0'>
                    <p className='mb-10 text-xl font-bold text-center md:text-left md:leading-[50px] md:text-40'>
                        Interested in our product?<br />
                        Request a Demo now!
                    </p>
                    <a href="#" className=''>
                        <p
                            className='rounded-[20px] w-[212px] h-[60px] text-sm font-semibold bg-purple-510 flex justify-center items-center text-white font-open-sans'>
                            Request a Demo</p>
                    </a>
                </div>
                <div className='md:h-[416px]'>
                    <Image src="/imgs/commu.png" sizes="100vw" width="0" height="0"
                        className='md:w-[735px] w-full md:h-[416px] h-auto md:absolute right-0' alt="" />
                </div>
            </div>
        </div>
    </section>
</Layout>
);
}