import Image from 'next/image';
import Link from 'next/link'
import Script from 'next/script'
import { Dropdown } from "@nextui-org/react";

export default function Nav() {
    return (
        <div className="fixed top-0 z-50 flex items-center w-full h-20 bg-black bg-opacity-50">
            <div className="max-w-[1600px] mx-auto w-full md:px-20 px-5">
                <nav className="border-gray-210">
                    <div className="flex flex-wrap items-center justify-between mx-auto ">
                        <div className='w-[50px] h-[40px] relative'>
                            <Link href="/">
                                <Image className="w-[50px]" src="/imgs/logo.svg" alt="" fill />
                            </Link>
                        </div>
                        <Dropdown style={{ borderRadius: 0 }} closeOnSelect={false}>
                            <Dropdown.Trigger>
                                <button type="button"
                                    className="inline-flex items-center justify-center ml-3 text-gray-400 rounded-lg md:hidden hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-blue-300"
                                >
                                    <span className="sr-only">Open main menu</span>
                                    <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd"
                                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                                            clipRule="evenodd"></path>
                                    </svg>
                                    <svg className="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd"
                                            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                            clipRule="evenodd"></path>
                                    </svg>
                                </button>
                            </Dropdown.Trigger>
                            <Dropdown.Menu>
                                <Dropdown.Item key="sp-home"><Link href="/" className="text-base text-center  font-montserrat">Home</Link></Dropdown.Item>
                                <Dropdown.Item key="sp-solution">
                                    <Dropdown style={{ borderRadius: 0 }}>
                                        <Dropdown.Trigger>
                                            <div
                                                className="border-gray-100 flex items-center justify-center md:border-0 md:p-0  md:w-auto w-full"><span className='text-white'>Solutions</span>
                                                <svg className="w-4 h-4 ml-1 text-white" fill="currentColor" viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd"
                                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                        clipRule="evenodd"></path>
                                                </svg>
                                            </div>
                                        </Dropdown.Trigger>
                                        <Dropdown.Menu>
                                            <Dropdown.Item key="ainterview"><Link href="/interview" className="text-base text-center  font-montserrat">Interview</Link></Dropdown.Item>
                                            <Dropdown.Item key="aemployees-management"><Link href="/employees-management" className="text-base text-center  font-montserrat">Employees Management</Link></Dropdown.Item>
                                            <Dropdown.Item key="apayroll"><Link href="/payroll" className="text-base text-center  font-montserrat">Payroll</Link></Dropdown.Item>
                                            <Dropdown.Item key="aassets-management"><Link href="/assets-management" className="text-base text-center  font-montserrat">Assets Management</Link></Dropdown.Item>
                                            <Dropdown.Item key="aattendance"><Link href="/attendance" className="text-base text-center  font-montserrat">Attendance</Link></Dropdown.Item>
                                            <Dropdown.Item key="ashift-planning"><Link href="/shift-planning" className="text-base text-center  font-montserrat">Shift Planning</Link></Dropdown.Item>
                                            <Dropdown.Item key="aclaims"><Link href="/claims" className="text-base text-center  font-montserrat">Claims</Link></Dropdown.Item>
                                            <Dropdown.Item key="aleaves"><Link href="/leaves" className="text-base text-center  font-montserrat">Leaves</Link></Dropdown.Item>
                                            <Dropdown.Item key="aappraisal-evaluation"><Link href="/appraisal-evaluation" className="text-base text-center  font-montserrat">Appraisal Evaluation</Link></Dropdown.Item>
                                            <Dropdown.Item key="abooking"><Link href="/desk-room-booking" className="text-base text-center  font-montserrat">Desk & Room Booking</Link></Dropdown.Item>
                                            <Dropdown.Item key="achat"><Link href="/chat" className="text-base text-center  font-montserrat">Chat</Link></Dropdown.Item>
                                            <Dropdown.Item key="astaff"><Link href="/mobile-app" className="text-base text-center  font-montserrat">Staff Mobile App</Link></Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </Dropdown.Item>
                                <Dropdown.Item key="sp-plan"><Link href="/plan" className="text-base text-center  font-montserrat">Plans</Link></Dropdown.Item>
                                <Dropdown.Item key="sp-about"><Link href="/about" className="text-base text-center  font-montserrat">About</Link></Dropdown.Item>
                                <Dropdown.Item key="sp-contact"><Link href="/contact" className="text-base text-center  font-montserrat">Contact</Link></Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>

                        <div className="absolute top-10 md:top-auto ml-[-20px] px-5 hidden w-full md:relative md:block md:w-auto"
                            id="mobile-menu">
                            <ul
                                className="flex flex-col mt-4 md:flex-row md:space-x-[60px] md:mt-0 md:text-sm md:font-medium bg-white md:bg-transparent">
                                <li>
                                    <Link href="/"
                                        className="block py-2 text-base font-montserrat pl-3 pr-4 text-black rounded md:bg-transparent md:text-blue-210 md:p-0 focus:outline-none"
                                        aria-current="page">Home</Link>
                                </li>
                                <li className='menu-dropdown'>
                                    <Dropdown style={{ borderRadius: 0 }}>
                                        <Dropdown.Trigger>
                                            <div
                                                className="flex text-base items-center font-montserrat justify-between w-full py-2 pl-3 pr-4 font-medium text-black border-b border-gray-100 md:text-white md:border-0 md:p-0 md:w-auto">Solutions
                                                <svg className="w-4 h-4 ml-1" fill="currentColor" viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd"
                                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                        clipRule="evenodd"></path>
                                                </svg>
                                            </div>
                                        </Dropdown.Trigger>
                                        <Dropdown.Menu>
                                            <Dropdown.Item key="interview"><Link href="/interview" className=" text-center  text-base font-montserrat">Interview</Link></Dropdown.Item>
                                            <Dropdown.Item key="employees-management"><Link href="/employees-management" className=" text-center  text-base font-montserrat">Employees Management</Link></Dropdown.Item>
                                            <Dropdown.Item key="payroll"><Link href="/payroll" className=" text-center  text-base font-montserrat">Payroll</Link></Dropdown.Item>
                                            <Dropdown.Item key="assets-management"><Link href="/assets-management" className=" text-center  text-base font-montserrat">Assets Management</Link></Dropdown.Item>
                                            <Dropdown.Item key="attendance"><Link href="/attendance" className=" text-center  text-base font-montserrat">Attendance</Link></Dropdown.Item>
                                            <Dropdown.Item key="shift-planning"><Link href="/shift-planning" className=" text-center  text-base font-montserrat">Shift Planning</Link></Dropdown.Item>
                                            <Dropdown.Item key="claims"><Link href="/claims" className=" text-center  text-base font-montserrat">Claims</Link></Dropdown.Item>
                                            <Dropdown.Item key="leaves"><Link href="/leaves" className=" text-center  text-base font-montserrat">Leaves</Link></Dropdown.Item>
                                            <Dropdown.Item key="appraisal-evaluation"><Link href="/appraisal-evaluation" className=" text-center  text-base font-montserrat">Appraisal Evaluation</Link></Dropdown.Item>
                                            <Dropdown.Item key="booking"><Link href="/desk-room-booking" className=" text-center  text-base font-montserrat">Desk & Room Booking</Link></Dropdown.Item>
                                            <Dropdown.Item key="chat"><Link href="/chat" className=" text-center  text-base font-montserrat">Chat</Link></Dropdown.Item>
                                            <Dropdown.Item key="staff"><Link href="/mobile-app" className=" text-center  text-base font-montserrat">Staff Mobile App</Link></Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </li>
                                <li>
                                    <Link href="/plan"
                                        className="block text-base font-montserrat py-2 pl-3 pr-4 text-black border-b border-gray-100 md:text-white md:border-0 md:p-0">Plans</Link>
                                </li>
                                <li>
                                    <Link href="/about"
                                        className="block text-base font-montserrat py-2 pl-3 pr-4 text-black border-b border-gray-100 md:text-white md:border-0 md:p-0">About</Link>
                                </li>
                                <li>
                                    <Link href="/contact"
                                        className="block text-base font-montserrat py-2 pl-3 pr-4 text-black border-b border-gray-100 md:text-white md:border-0 md:p-0">Contact</Link>
                                </li>
                                <li>
                                    <a href="#"
                                        className="block text-base  py-2 pl-3 pr-4 text-black border-b border-gray-100 md:text-white md:border-0 md:p-0 ">
                                        <label
                                            className="px-8 py-1 text-black rounded-full cursor-pointer bg-blue-210 font-open-sans">Login</label>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <Script src="https://unpkg.com/@themesberg/flowbite@1.1.1/dist/flowbite.bundle.js" />
            <Script src="https://unpkg.com/flowbite@1.4.0/dist/flowbite.js" />
        </div>
    );
}