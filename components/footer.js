import Image from 'next/image';
import Link from 'next/link'
import Script from 'next/script'

export default function Footer() {
    return (
        <section>
            <footer className="py-10 bg-black">
                <div className="container mx-auto">
                    <div className="grid md:grid-cols-5">
                        <div>
                            <div className='relative w-[100px] h-20 mb-[30px]'>
                                <Image src="/imgs/logo.svg" className="" fill alt="" />
                            </div>
                            <p className="mb-2 font-bold text-white font-inter">Contact Info</p>
                            <p className="mb-2 text-white font-inter">8600 3811</p>
                            <p className="mb-2 text-white font-inter">info@kelick.io</p>
                        </div>
                        <div>
                            <p className="mb-3 font-bold text-white font-inter">Quick Links</p>
                            <p className="mb-3 text-white font-inter"><Link href="/">Home</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/plan">Plans</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/about">About</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/contact">Contact Us</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href='/privacy-policy'>Privacy Policy</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href='/terms-condiitons'>Terms & Conditions</Link></p>
                        </div>
                        <div>
                            <p className="mb-3 font-bold text-white font-inter">Solutions</p>
                            <p className="mb-3 text-white font-inter"><Link href="/interview">Interview</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/employees-management">Employees Management</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/payroll">Payroll</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/assets-management">Assets Management</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/attendance">Attendance</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/shift-planning">Shift Planning</Link></p>
                        </div>
                        <div className='flex flex-col h-full justify-end'>
                            <p className="mb-3 text-white font-inter"><Link href="/claims">Claims</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/leaves">Leaves</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/appraisal-evaluation">Appraisal Evaluation</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/desk-room-booking">Desk & Room Booking</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/chat">Chat</Link></p>
                            <p className="mb-3 text-white font-inter"><Link href="/mobile-app">Staff Mobile App</Link></p>
                        </div>
                        <div>
                            <p className="mb-3 font-bold text-white font-inter">Stay Connected</p>
                            <div className="flex mt-4">
                                <div className='w-[30px] h-[30px] mr-5 relative'>
                                    <Image src="/imgs/instagram.svg" fill alt="" />
                                </div>
                                <div className='w-[30px] h-[30px] mr-5 relative'>
                                    <Image src="/imgs/facebook.svg" fill alt="" />
                                </div>
                            </div>
                            <div className="mt-4 md:mt-16">
                                <p className="text-[10px] text-white font-inter mb-2">Supported by</p>
                                <div className='relative'>
                                    <Image src="/imgs/sme.png" sizes="100vw" width="0" height="0"
                                        className='md:w-[118px] w-full h-auto ' alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr className="h-[1px] bg-white my-5" />
                    <div className="flex flex-col justify-between md:flex-row">
                        <p className="mb-3 text-white font-inter md:mb-0">Copyright© Kelick All Rights Reserved.</p>
                        <div className="flex flex-col md:items-center md:flex-row">
                            <div className='relative h-8 mr-1 w-14'>
                                <Image src="/imgs/safe.png" alt="" fill />
                            </div>
                            <p className="text-sm text-white font-poppins">ENTERPRISE ON ASSET-BASED CYBER DEFENCE AN INITIATIVE BY CSA</p>
                        </div>
                    </div>
                </div>
            </footer>
        </section>
    );
}