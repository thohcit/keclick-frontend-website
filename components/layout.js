import Head from 'next/head';
import Nav from './nav';
import Footer from './footer';
export default function Layout({ children }) {
    return (
        <>
            <Head>
                <title>Keclick</title>
                <link rel="icon" href="/imgs/logo-icon.svg" />
            </Head>
            <div>
                <Nav></Nav>
                {children}
                <Footer></Footer>
            </div>
        </>
    );
}