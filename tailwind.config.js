/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./app/**/*.{js,ts,jsx,tsx}",
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        screens: {
            md: '954px',
        },

        extend: {
            screens: {
                'lg': '1200px',
            },
            fontFamily: {
                'sans': ['Quicksand'],
                'open-sans': ['Open Sans'],
                'inter': ['Inter'],
                'poppins': ['Poppins'],
                'montserrat': ['Montserrat'],
            },
            fontSize: {
                '10': '10px',
                '32': '32px',
                '60': '60px',
                '95': '95px',
            },
            container: {
                padding: {
                    DEFAULT: '20px',
                    md: '0',
                    lg: '0'
                },
            },
            dropShadow: {
                xs: '0 25px 30px rgba(0, 209, 198, 0.15)',
            },
            fontSize: {
                22: '22px',
                36: '36px',
                40: '40px',
                72: '72px',
                80: '80px',
                120: '120px',
            },
            backgroundImage: {
                'top-banner': "url('/imgs/top-bg.png')",
                'interview': "url('/imgs/interview-bg.png')",
                'employees-management': "url('/imgs/employees-management-bg.png')",
                'application': "url('/imgs/application.png')",
                'payroll': "url('/imgs/payroll-bg.png')",
                'asset-management': "url('/imgs/asset-management.png')",
                'attendance': "url('/imgs/attendance-bg.png')",
                'shift-planning': "url('/imgs/shift-planning-bg.png')",
                'claims': "url('/imgs/claims-bg.png')",
                'leaves': "url('/imgs/leaves-bg.png')",
                'appraisal-evaluation': "url('/imgs/appraisal-evaluation-bg.png')",
                'desk-room-booking': "url('/imgs/desk-room-booking.png')",
                'chat': "url('/imgs/chat-bg.png')",
                'mobile-app': "url('/imgs/mobile-app-bg.png')",
                'about': "url('/imgs/about-bg.png')",
                'contact': "url('/imgs/contact-bg.png')",
                'plan': "url('/imgs/plan-bg.png')",
                'landing-page': "url('/imgs/landing-page-bg.png')",
            },
            colors: {
                blue: {
                    210: '#00D1C6',
                    310: '#00CDC2',
                },
                gray: {
                    210: '#808080',
                    110: '#F2F2F2',
                    310: '#D9D9D9',
                    810: '#808080',
                    950: '#666666',
                },
                purple: {
                    210: '#DEDDF7',
                    510: '#5856D6',
                },
                yellow: {
                    550: '#FFB600'
                }
            },
        },
    },
}
